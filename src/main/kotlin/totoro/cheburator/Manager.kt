package totoro.cheburator

/**
 * Задача менеджера:
 * 1) подключиться к API чатика
 * 2) принимать сообщения и передавать их в обе стороны
 */
abstract class Manager {
    /**
     * Принимает сообщение извне и затем отсылает его через API
     */
    abstract fun send(message: Message)

    var listener: ((Message) -> Unit)? = null

    /**
     * Запоминает переданный листенер и затем дергает его каждый раз, как из API приходит сообщение
     */
    fun subscribe(listener: (Message) -> Unit) {
        this.listener = listener
    }
}
