package totoro.cheburator

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.InlineDataPart
import com.github.kittinunf.result.Result
import org.jetbrains.kotlin.backend.common.push
import org.jetbrains.kotlin.utils.newHashMapWithExpectedSize
import org.kitteh.irc.client.library.util.Cutter
import org.kitteh.irc.client.library.util.Format

const val CLBIN_URL = "https://clbin.com/"
val OVERFLOW_URL_AVAILABLE = "${Format.COLOR_CHAR}${Format.LIGHT_GRAY}%d lines hidden:${Format.RESET} %s"
val OVERFLOW_NO_URL =
    "${Format.COLOR_CHAR}${Format.LIGHT_GRAY}%d lines hidden: ${Format.COLOR_CHAR}${Format.RED}could not get the URL"

fun getByteLength(char: Char): Int {
    val byte = char.code

    return when {
        byte < 0x80 -> 1
        byte < 0x800 -> 2
        byte < 0x10000 -> 3
        else -> 4
    }
}

fun sendToClbin(data: String): String? {
    val (_, _, result) = Fuel.upload(CLBIN_URL)
        .add(InlineDataPart(data, name = "clbin"))
        .responseString()

    return when (result) {
        is Result.Failure -> {
            print(result.getException())
            null
        }
        is Result.Success -> {
            result.get().trim()
        }
    }
}

fun getByteLength(str: String): Int = str.map { getByteLength(it) }.sum()

open class ByteAwareIrcMessageCutter : Cutter {
    override fun split(message: String, size: Int): MutableList<String> {
        val messages = mutableListOf<String>()

        var lineStart = 0
        var pos = -1
        var bytePos = 0
        var lastSpacePos = -1
        var spaceSeq = false
        var forcedLineBreak = false

        fun firstLine() = messages.size == 0

        fun pushLine(line: String) {
            messages.push(line)

            lastSpacePos = -1
            bytePos = 0
            spaceSeq = false
            forcedLineBreak = false
        }

        while (pos + 1 < message.length) {
            pos += 1
            val char = message[pos]

            if (char == ' ' && pos == lineStart && !firstLine() && !forcedLineBreak) {
                // Skip trailing spaces
                lineStart += 1
                continue
            }

            if (char != ' ') {
                spaceSeq = false
            }

            if (char == '\n') {
                pushLine(message.substring(lineStart, pos))
                lineStart = pos + 1
                forcedLineBreak = true
            }

            if (char == ' ' && !spaceSeq) {
                lastSpacePos = pos
                spaceSeq = true
            }

            bytePos += getByteLength(char)

            if (bytePos > size) {
                if (lastSpacePos >= (pos - lineStart) / 2) {
                    val line = message.substring(lineStart, lastSpacePos)
                    lineStart = lastSpacePos + 1
                    pos = lastSpacePos

                    pushLine(line)
                } else {
                    val line = message.substring(lineStart, pos)
                    lineStart = pos
                    pos -= 1

                    pushLine(line)
                }

                continue
            }
        }

        if (lineStart != pos + 1) {
            pushLine(message.substring(lineStart))
        }

        return messages
    }
}

open class ByteAwareOverflowingIrcMessageCutter(private val maxLines: Int) : ByteAwareIrcMessageCutter() {
    override fun split(message: String, size: Int): MutableList<String> {
        val messages = super.split(message, size)

        val overflow = messages.size - maxLines + 1

        if (messages.size > maxLines) {
            for (i in (messages.size - maxLines + 1) downTo 1) {
                messages.removeAt(maxLines - 1)
            }

            val url = sendToClbin(message)

            messages.push(
                if (url != null) {
                    OVERFLOW_URL_AVAILABLE.format(overflow, url)
                } else {
                    OVERFLOW_NO_URL.format(overflow)
                }
            )
        }

        return messages
    }
}

class PrefixingIrcMessageCutter(
    maxLines: Int,
    private val segmentStyle: SegmentStyle,
    private val prefix: String
) : ByteAwareOverflowingIrcMessageCutter(maxLines) {
    enum class SegmentStyle(
        private val key: String,
        val topSegment: String,
        val middleSegment: String,
        val bottomSegment: String,
        val defaultDelimiter: String
    ) {
        Line("line", "╷ ", "│ ", "╵ ", ": "),
        SquareBracket("square-bracket", " ⎡ ", " ⎢ ", " ⎣ ", ": "),
        Integral("integral", " ⌠ ", " ⎮ ", " ⌡ ", ": ");

        companion object {
            private val keyMapping: MutableMap<String, SegmentStyle> = newHashMapWithExpectedSize(values().size)

            init {
                values().forEach { keyMapping[it.key] = it }
            }

            fun byKey(key: String): SegmentStyle? = keyMapping[key]
        }
    }

    private val prefixLength = getByteLength(prefix)
    private val delimiterLength = getByteLength(segmentStyle.middleSegment)

    override fun split(message: String, size: Int): MutableList<String> {
        val messages = super.split(message, size - prefixLength - delimiterLength)

        if (messages.size <= 1) {
            val body = prefix + segmentStyle.defaultDelimiter + (messages.getOrNull(0) ?: "")

            if (messages.size == 1) {
                messages[0] = body
            } else {
                messages.add(body)
            }
        } else {
            messages[0] = prefix + segmentStyle.topSegment + messages[0]
            messages[messages.size - 1] = prefix + segmentStyle.bottomSegment + messages[messages.size - 1]

            for (i in 1 until messages.size - 1) {
                messages[i] = prefix + segmentStyle.middleSegment + messages[i]
            }
        }

        return messages
    }
}