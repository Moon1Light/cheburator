package totoro.cheburator

import java.io.FileInputStream
import java.util.*

class Config(filename: String) {
    private val prop = Properties()

    var nickname = "cheburator"
    var nickservAccount: String? = null
    var nickservPassword: String? = null
    var ircChannel = "#cc.ru"
    var ircHost = "irc.esper.net"
    var discordChannel = "irc-discord"
    var token: String? = null
    var maxLines = 5
    var sendDelay = 1000 / maxLines
    var segmentStyle = PrefixingIrcMessageCutter.SegmentStyle.Line

    init {
        FileInputStream(filename).use {
            prop.load(it)
            nickname = getString(NICKNAME_KEY) ?: nickname
            nickservAccount = getString(NICKSERV_ACCOUNT_KEY) ?: nickservAccount
            nickservPassword = getString(NICKSERV_PASSWORD_KEY) ?: nickservPassword
            ircChannel = getString(IRC_CHANNEL_KEY) ?: ircChannel
            ircHost = getString(IRC_HOST_KEY) ?: ircHost
            discordChannel = getString(DISCORD_CHANNEL_KEY) ?: discordChannel
            token = getString(TOKEN_KEY)
            maxLines = getInt(MAX_LINES_KEY) ?: maxLines
            sendDelay = getInt(SEND_DELAY_KEY) ?: sendDelay
            segmentStyle = getString(SEGMENT_STYLE)?.let { key ->
                PrefixingIrcMessageCutter.SegmentStyle.byKey(key)
                    ?: throw IllegalArgumentException("invalid segment style $key")
            } ?: segmentStyle
        }
    }

    private fun getString(key: String): String? = prop.getProperty(key)

    private fun getInt(key: String): Int? = prop.getProperty(key)?.toIntOrNull()

    companion object {
        private const val NICKNAME_KEY = "nickname"
        private const val NICKSERV_ACCOUNT_KEY = "nickserv-account"
        private const val NICKSERV_PASSWORD_KEY = "nickserv-password"
        private const val IRC_CHANNEL_KEY = "irc-channel"
        private const val IRC_HOST_KEY = "irc-host"
        private const val DISCORD_CHANNEL_KEY = "discord-channel"
        private const val TOKEN_KEY = "token"
        private const val MAX_LINES_KEY = "max-lines"
        private const val SEND_DELAY_KEY = "send-delay"
        private const val SEGMENT_STYLE = "segment-style"
    }
}
