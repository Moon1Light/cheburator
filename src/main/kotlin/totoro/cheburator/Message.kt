package totoro.cheburator

sealed class Message {

    data class Regular(val author: String, val text: String, val referencedMessage: Message?) : Message()
    data class Action(val author: String, val action: String) : Message()
    data class Join(val user: String) : Message()

    data class Quit(val user: String, val reason: Reason) : Message() {
        sealed interface Reason {
            data class Part(val message: String) : Reason
            data class Quit(val message: String) : Reason
            data class Kick(val actor: String, val message: String) : Reason
        }
    }
}