package totoro.cheburator

import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message.Attachment
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.cache.CacheFlag
import net.dv8tion.jda.api.entities.Message as DiscordMessage

class DiscordManager(token: String, private val channel: String) : Manager() {
    private var lastUsers = mutableMapOf<String, String>()

    private val intents = listOf(
        GatewayIntent.GUILD_MEMBERS,
        GatewayIntent.GUILD_MESSAGES,
        GatewayIntent.GUILD_EMOJIS_AND_STICKERS,
        GatewayIntent.MESSAGE_CONTENT
    )
    private val jda = JDABuilder.create(token, intents)
        .disableCache(
            CacheFlag.ACTIVITY,
            CacheFlag.VOICE_STATE,
            CacheFlag.CLIENT_STATUS,
            CacheFlag.ONLINE_STATUS,
            CacheFlag.MEMBER_OVERRIDES,
            CacheFlag.SCHEDULED_EVENTS,
            CacheFlag.EMOJI,
        )
        .addEventListeners(object : ListenerAdapter() {
            override fun onMessageReceived(event: MessageReceivedEvent) {
                if (listener != null) {
                    if (event.author != event.jda.selfUser && event.channel.name == channel) {
                        val message = event.message
                        updateMemberCache(message)
                        listener?.invoke(convertMessage(message))
                    }
                }
            }
        })
        .setActivity(Activity.watching("IRC"))
        .build()

    private fun updateMemberCache(event: DiscordMessage) {
        val member = event.member ?: return

        lastUsers[member.effectiveName] = member.id
    }

    private fun convertMessage(message: DiscordMessage): Message {
        val name = resolveAuthor(message)
        val text = handleAttachments(message)
        val referencedMessage = message.referencedMessage?.let(::convertMessage)

        return Message.Regular(name, text, referencedMessage)
    }

    override fun send(message: Message) {
        jda.getTextChannelsByName(channel, false).forEach {
            when (message) {
                is Message.Regular -> {
                    val text = resolveMentions(it.guild, message.text)
                    it.sendMessage("**${message.author}:** $text").submit()
                }

                is Message.Action -> {
                    val text = resolveMentions(it.guild, message.action)
                    it.sendMessage("* **${message.author}** $text").submit()
                }

                is Message.Join -> it.sendMessage("**__${message.user}__ has joined the channel.**").submit()

                is Message.Quit -> when (message.reason) {
                    is Message.Quit.Reason.Quit ->
                        it.sendMessage("**__${message.user}__ has quit the server:** ${message.reason.message}.")
                            .submit()

                    is Message.Quit.Reason.Part ->
                        it.sendMessage("**__${message.user}__ has left the channel:** ${message.reason.message}.")
                            .submit()

                    is Message.Quit.Reason.Kick ->
                        it.sendMessage(
                            "**__${message.user}__ has been kicked from the channel " +
                                    "by __${message.reason.actor}__:** ${message.reason.message}"
                        ).submit()
                }
            }
        }
    }

    private fun resolveAuthor(event: DiscordMessage): String {
        val member = event.member

        return member?.effectiveName ?: event.author.name
    }

    // accepts any of the following formats:
    // - @name
    // - @[name]
    // - @[====[name]====]
    // the multi-bracketed variants match the opening [====[ with the closest closing multi-bracket ]====]
    // that has the same number of equal signs (possibly zero)
    private val mentionRegex = Regex("@(?:((?:_|[^\\p{Punct}\\s])+)|\\[(?![\\[=])([^]]+)]|\\[(=*)\\[(.+?)]\\3])")

    private fun resolveMentions(guild: Guild, message: String): String = mentionRegex.replace(message) replace@{
        val name = it.groupValues[1]
            .ifEmpty { it.groupValues[2] }
            .ifEmpty { it.groupValues[4] }
        val id = getIdFromName(guild, name) ?: return@replace it.value

        // update the cache here as well for convenience
        lastUsers[name] = id

        return@replace "<@$id>"
    }

    private fun getIdFromName(guild: Guild, name: String): String? {
        // once we're here, the discriminator doesn't matter
        val candidates = guild.getMembersByNickname(name, true)
            .ifEmpty { guild.getMembersByEffectiveName(name, true) }
            .ifEmpty { guild.getMembersByName(name, true) }

        val disambiguatedId = lastUsers[name].takeIf { candidates.size > 1 }

        return disambiguatedId ?: candidates.firstOrNull()?.id
    }

    private fun handleAttachments(message: DiscordMessage): String {
        val body = message.contentDisplay.takeIf { it.isNotEmpty() }
        val attachments = message.attachments.joinToString(" ") { it.resolvedUrl }.takeIf { it.isNotEmpty() }
        val stickers = message.stickers.joinToString(" ") { "[sticker: ${it.name}]" }.takeIf { it.isNotEmpty() }

        return join(join(body, attachments, " "), stickers, "\n") ?: ""
    }

    private fun join(lhs: String?, rhs: String?, separator: String): String? {
        return when {
            lhs != null && rhs != null -> "$lhs$separator$rhs"
            lhs != null -> lhs
            rhs != null -> rhs
            else -> null
        }
    }

    private val Attachment.resolvedUrl: String
        get() = if (width != -1 && height != -1) proxyUrl else url
}
