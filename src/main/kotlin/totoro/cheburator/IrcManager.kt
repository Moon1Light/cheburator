package totoro.cheburator

import net.engio.mbassy.listener.Handler
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.element.User
import org.kitteh.irc.client.library.event.channel.ChannelCtcpEvent
import org.kitteh.irc.client.library.event.channel.ChannelJoinEvent
import org.kitteh.irc.client.library.event.channel.ChannelKickEvent
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent
import org.kitteh.irc.client.library.event.channel.ChannelPartEvent
import org.kitteh.irc.client.library.event.client.ClientNegotiationCompleteEvent
import org.kitteh.irc.client.library.event.user.UserQuitEvent
import org.kitteh.irc.client.library.feature.filter.MeCommandOnly
import org.kitteh.irc.client.library.feature.sending.SingleDelaySender
import org.kitteh.irc.client.library.util.Format

const val NICKSERV = "NickServ"
const val NICKSERV_IDENTIFY = "IDENTIFY %s %s"

fun stripFormatting(msg: String): String {
    return Format.stripAll(msg).replace(Regex("[\u0000-\u001f]"), "")
}

const val MAX_QUOTE_SIZE = 300

class IrcManager(
    host: String,
    private val channel: String,
    nickname: String,
    private val nickservAccount: String?,
    private val nickservPassword: String?,
    private val maxLines: Int,
    sendDelay: Int,
    private val segmentStyle: PrefixingIrcMessageCutter.SegmentStyle,
) : Manager() {
    private val client: Client = Client.builder()
        .nick(nickname)
        .server()
        .host(host)
        .then()
        .management()
        .messageSendingQueueSupplier(SingleDelaySender.getSupplier(sendDelay))
        .then()
        .buildAndConnect()
    private val lastReferencedAuthors = mutableMapOf<String, String>()

    init {
        client.eventManager.registerEventListener(this)
        client.addChannel(channel)

        // Set the default cutter without the overflow logic
        client.messageCutter = ByteAwareIrcMessageCutter()
    }

    override fun send(message: Message) {
        if (message is Message.Regular) {
            if (message.referencedMessage is Message.Regular) {
                val text = message.referencedMessage.text
                val nickname = text.substring(2 until message.referencedMessage.text.indexOf(':'))
                lastReferencedAuthors[nickname] = message.author
            }

            val prefix = "${IrcNickFormatter.format(message.author)}${Format.RESET}"
            val cutter = PrefixingIrcMessageCutter(maxLines, segmentStyle, prefix)
            val referencedMessage = formatReferencedMessage(message.referencedMessage)

            val text = "$referencedMessage${message.text}"
            client.sendMultiLineMessage(channel, text, cutter)
        }
    }

    private fun formatReferencedMessage(it: Message?): String {
        val (author, quote) = when (it) {
            is Message.Regular -> Pair(it.author, it.text)
            is Message.Action -> Pair(it.author, it.action)
            else -> Pair(null, null)
        }

        return if (author == null) {
            ""
        } else {
            val splitQuote = ByteAwareIrcMessageCutter().split(quote!!, MAX_QUOTE_SIZE)
            val cutQuote = splitQuote[0] + (if (splitQuote.size > 1) "…" else "")

            "${Format.LIGHT_GRAY}╭─ ${Format.LIGHT_GRAY}${
                IrcNickFormatter.format(author, false)
            }${Format.RESET}${Format.LIGHT_GRAY}: $cutQuote\n"
        }
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        lastReferencedAuthors[event.actor.nick]?.let {
            val message = event.message.replace("\$lr", "@${it}").replace(";по", "@${it}")
            listener?.invoke(Message.Regular(event.actor.nick, stripFormatting(message), null))
        } ?: run {
            listener?.invoke(Message.Regular(event.actor.nick, stripFormatting(event.message), null))
        }
    }

    @Handler
    @MeCommandOnly
    fun action(event: ChannelCtcpEvent) {
        val nick = event.actor.nick
        val action = stripFormatting(event.message).split(" ", limit = 2).getOrNull(1) ?: ""

        listener?.invoke(Message.Action(nick, action))
    }

    @Handler
    fun join(event: ChannelJoinEvent) {
        listener?.invoke(Message.Join(event.actor.nick))
    }

    @Handler
    fun part(event: ChannelPartEvent) {
        listener?.invoke(Message.Quit(event.actor.nick, Message.Quit.Reason.Part(event.message)))
    }

    @Handler
    fun quit(event: UserQuitEvent) {
        listener?.invoke(Message.Quit(event.actor.nick, Message.Quit.Reason.Quit(event.message)))
    }

    @Handler
    fun kick(event: ChannelKickEvent) {
        val actor = (event.actor as? User)?.nick ?: event.actor.name
        listener?.invoke(Message.Quit(event.target.nick, Message.Quit.Reason.Kick(actor, event.message)))
    }

    @Handler
    fun identify(@Suppress("UNUSED_PARAMETER") event: ClientNegotiationCompleteEvent) {
        if (nickservAccount != null && nickservPassword != null) {
            client.sendMessage(NICKSERV, NICKSERV_IDENTIFY.format(nickservAccount, nickservPassword))
        }
    }
}
